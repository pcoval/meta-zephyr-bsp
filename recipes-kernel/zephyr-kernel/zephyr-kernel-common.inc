# Common settings for all Zephyr recipes

ZEPHYR_INHERIT_CLASSES += "zephyr cmake"
inherit ${ZEPHYR_INHERIT_CLASSES}
inherit python3native

# There shouldn't be a manifest for zephyr kernels since there is no root
# filesystem.
IMAGE_NO_MANIFEST = "1"

ZEPHYR_GCC_VARIANT="yocto"
ZEPHYR_SYSROOT="${STAGING_DIR_TARGET}"

ZEPHYR_MAKE_OUTPUT = "zephyr.elf"
ZEPHYR_MAKE_BIN_OUTPUT = "zephyr.bin"
ZEPHYR_MAKE_EFI_OUTPUT = "zephyr.efi"

EXTRA_OECMAKE = "\
    -DZEPHYR_BASE=${S} \
    -DZEPHYR_GCC_VARIANT=yocto \
    -DBOARD=${BOARD} \
    -DARCH=${ARCH} \
    -DCROSS_COMPILE=${CROSS_COMPILE} \
    -DZEPHYR_SYSROOT=${ZEPHYR_SYSROOT} \
    -DZEPHYR_TOOLCHAIN_VARIANT=yocto \
    -DEXTRA_CPPFLAGS=${CPPFLAGS} \
    "

ZEPHYR_MODULES = ""
#SOC_FAMILY
ZEPHYR_MODULES_append_arm = "\;${S}/modules/cmsis"
ZEPHYR_MODULES_append_atmel = "\;${S}/modules/hal/atmel"
ZEPHYR_MODULES_append_cypress = "\;${S}/modules/hal/cypress"
ZEPHYR_MODULES_append_infineon = "\;${S}/modules/hal/infineon"
ZEPHYR_MODULES_append_microchip = "\;${S}/modules/hal/microchip"
ZEPHYR_MODULES_append_nordic = "\;${S}/modules/hal/nordic"
ZEPHYR_MODULES_append_nuvoton = "\;${S}/modules/hal/nuvoton"
ZEPHYR_MODULES_append_nxp = "\;${S}/modules/hal/nxp"
ZEPHYR_MODULES_append_stm32 = "\;${S}/modules/hal/stm32"
ZEPHYR_MODULES_append_silabs = "\;${S}/modules/hal/silabs"
ZEPHYR_MODULES_append_ti = "\;${S}/modules/hal/ti"


#ACTUAL MODULES
ZEPHYR_MODULES_append_zopenamp = "\;${S}/modules/lib/open-amp\;${S}/modules/hal/libmetal"
ZEPHYR_MODULES_append_zmbedtls = "\;${S}/modules/crypto/mbedtls"
ZEPHYR_MODULES_append_zsof = "\;${S}/modules/audio/sof"
ZEPHYR_MODULES_append_ztracerecorder = "\;${S}/modules/debug/TraceRecorder"
ZEPHYR_MODULES_append_ztfm = "\;${S}/modules/tee/tfm"
ZEPHYR_MODULES_append_znanopb = "\;${S}/modules/lib/nanopb"
ZEPHYR_MODULES_append_ztensorflow = "\;${S}/modules/lib/tensorflow"
ZEPHYR_MODULES_append_zcanopen = "\;${S}/modules/lib/canopennode"
ZEPHYR_MODULES_append_zcivetweb = "\;${S}/modules/lib/civetweb"
ZEPHYR_MODULES_append_zsegger = "\;${S}/modules/debug/segger\;"

EXTRA_OECMAKE_append = " -DZEPHYR_MODULES=${ZEPHYR_MODULES}"

export ZEPHYR_BASE="${S}"

DEPENDS += "gperf-native python3-pyelftools-native python3-pyyaml-native python3-pykwalify-native \
            python3-lpc-checksum-native python3-intelhex-native python3-colorama-native"
CROSS_COMPILE = "${STAGING_BINDIR_TOOLCHAIN}/${TARGET_PREFIX}"

DEPENDS_append_qemuall = " qemu-native qemu-helper-native"

# The makefiles are explicit about the flags they want, so don't unset
# them so zephyr flags actually get used.
# This is done here rather than in the task so that things still work
# in devshell.

python () {
    d.delVar('CFLAGS')
    d.delVar('CXXFLAGS')
    d.delVar('LDFLAGS')
}

OE_TERMINAL_EXPORTS += "CROSS_COMPILE"
OE_TERMINAL_EXPORTS += "BOARD"
OE_TERMINAL_EXPORTS += "ZEPHYR_SRC_DIR"
OE_TERMINAL_EXPORTS += "ZEPHYR_BASE"
OE_TERMINAL_EXPORTS += "ZEPHYR_SYSROOT"
OE_TERMINAL_EXPORTS += "ZEPHYR_GCC_VARIANT"

IMAGE_FSTYPES = "elf bin"

do_configure_prepend() {
    # Zephyr expects CPPFLAGS as cmake argument as and ignores env variables.
    unset CPPFLAGS
}
