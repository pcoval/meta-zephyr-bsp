#
# Tune Settings for Cortex-R4
#
DEFAULTTUNE ?= "cortexr4"

TUNEVALID[cortexr4] = "Enable Cortex-R4 specific processor optimizations"
TUNE_CCARGS .= "${@bb.utils.contains('TUNE_FEATURES', 'cortexr4', ' -mcpu=cortex-r4', '', d)}"

require conf/machine/include/arm/arch-armv7r.inc

AVAILTUNES                            += "cortexr4"
ARMPKGARCH_tune-cortexr4               = "cortexr4"
TUNE_FEATURES_tune-cortexr4            = "${TUNE_FEATURES_tune-armv7r} cortexr4"
PACKAGE_EXTRA_ARCHS_tune-cortexr4      = "${PACKAGE_EXTRA_ARCHS_tune-armv7r} cortexr4"
