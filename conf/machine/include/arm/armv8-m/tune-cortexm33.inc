#
# Tune Settings for Cortex-M33
#
DEFAULTTUNE ?= "cortexm33"

TUNEVALID[cortexm33] = "Enable Cortex-M33 specific processor optimizations"
TUNE_CCARGS .= "${@bb.utils.contains('TUNE_FEATURES', 'cortexm33', ' -mcpu=cortex-m33', '', d)}"

require conf/machine/include/arm/arch-armv8m-main.inc

TUNEVALID[vfpv5spd16] = "Enable Vector Floating Point Version 5, Single Precision. with 16 registers (fpv5-sp-d16) unit."
TUNE_CCARGS_MFPU .= "${@bb.utils.contains('TUNE_FEATURES', 'vfpv5spd16', 'fpv5-sp-d16', '', d)}"

TUNE_CCARGS_MARCH_OPTS .= "${@bb.utils.contains('TUNE_FEATURES', [ 'vfpv3d16', 'vfpv5spd16' ], '+fp', '', d)}"

# GCC thnks that DSP and VFP are required, but Arm docs say it is
# optional.  So forcing below so that compiling works, but this should
# be fixed in GCC
AVAILTUNES                          += "cortexm33"
ARMPKGARCH_tune-cortexm33            = "cortexm33"
TUNE_FEATURES_tune-cortexm33         = "${TUNE_FEATURES_tune-armv8m-maine-vfpv5spd16} cortexm33"
PACKAGE_EXTRA_ARCHS_tune-cortexm33   = "${PACKAGE_EXTRA_ARCHS_tune-armv8m-maine-vfpv5spd16} cortexm33e-fpv5-spd16"
