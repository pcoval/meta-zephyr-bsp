#
# Tune Settings for Cortex-M35P
#
DEFAULTTUNE ?= "cortexm35p"

TUNEVALID[cortexm35p] = "Enable Cortex-M35p specific processor optimizations"
TUNE_CCARGS .= "${@bb.utils.contains('TUNE_FEATURES', 'cortexm35p', ' -mcpu=cortex-m35p', '', d)}"

require conf/machine/include/arm/arch-armv8m-main.inc

TUNEVALID[vfpv5spd16] = "Enable Vector Floating Point Version 5, Single Precision. with 16 registers (fpv5-sp-d16) unit."
TUNE_CCARGS_MFPU .= "${@bb.utils.contains('TUNE_FEATURES', 'vfpv5spd16', 'fpv5-sp-d16', '', d)}"

TUNE_CCARGS_MARCH_OPTS .= "${@bb.utils.contains('TUNE_FEATURES', [ 'vfpv3d16', 'vfpv5spd16' ], '+fp', '', d)}"

# GCC thnks that DSP and VFP are required, but Arm docs say it is
# optional.  So forcing below so that compiling works, but this should
# be fixed in GCC
AVAILTUNES                          += "cortexm35p"
ARMPKGARCH_tune-cortexm35p           = "cortexm35p"
TUNE_FEATURES_tune-cortexm35p        = "${TUNE_FEATURES_tune-armv8m-maine-vfpv5spd16} cortexm35p"
PACKAGE_EXTRA_ARCHS_tune-cortexm35p  = "${PACKAGE_EXTRA_ARCHS_tune-armv8m-maine-vfpv5spd16} cortexm35pe-fpv5-spd16"
